import React, {Component} from 'react';
import {connect} from 'react-redux';
import Task from '../../components/Task/Task';
import './TodoList.css';
import {changeValue, fetchTasks, fetchNewTask, removeTask} from "../../store/action";

class TodoList extends Component {
    componentDidMount() {
        this.props.fetchCounter();
    }

    addTask = event => {
        event.preventDefault();
        this.props.fetchNewTask();
    };

    render() {
        let tasks = (
            <div className="tasks">
                {this.props.tasks.map((task) => {
                    return <Task
                        key={task.id}
                        task={task.text}
                        remove={() => this.props.removeTask(task.id)}
                    />
                })}
            </div>
        );

        return (
            <div className="todo-list">
                <form action="#" className="task-form">
                    <input onChange={this.props.changeValue} type="text" placeholder="Add new task"/>
                    <button onClick={this.addTask}>Add</button>
                </form>
                {tasks}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        tasks: state.tasks,
        currentTask: state.currentTask
    }
};

const mapDispatchToProps = dispatch => {
    return {
        changeValue: (event) => dispatch(changeValue(event.target.value)),
        fetchCounter: () => dispatch(fetchTasks()),
        fetchNewTask: () => dispatch(fetchNewTask()),
        removeTask: (id) => dispatch(removeTask(id)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);