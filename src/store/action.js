import axios from '../axios-todo';

export const VALUE = 'VALUE';
export const FETCH_TASK_REQUEST = 'FETCH_TASK_REQUEST';
export const FETCH_TASK_SUCCESS = 'FETCH_TASK_SUCCESS';
export const FETCH_TASK_ERROR = 'FETCH_TASK_ERROR';

export const changeValue = (value) => {
    return {type: VALUE, value: value}
};

export const fetchTaskRequest = () => {
    return {type: FETCH_TASK_REQUEST};
};

export const fetchTaskSuccess = tasks => {
    return {type: FETCH_TASK_SUCCESS, tasks: tasks};
};

export const fetchTaskError = () => {
    return {type: FETCH_TASK_ERROR};
};

const getTasks = dispatch => {
    dispatch(fetchTaskRequest());
    axios.get('/tasks.json').then(response => {
        dispatch(fetchTaskSuccess(response.data));
    }, error => {
        dispatch(fetchTaskError());
    });
};

export const fetchTasks = () => {
    return dispatch => {
        getTasks(dispatch);
    }
};

export const fetchNewTask = () => {
    return (dispatch, getState) => {
        dispatch(fetchTaskRequest());
        axios.post('/tasks.json', {text: getState().currentTask}).then(() => {
            getTasks(dispatch);
        }, error => {
            dispatch(fetchTaskError());
        })
    }
};

export const removeTask = id => {
    console.log(id);
  return (dispatch) => {
      axios.delete(`/tasks/${id}.json`).then(() => {
          console.log(id);
          getTasks(dispatch);
      });
  }
};


