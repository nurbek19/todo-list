import {FETCH_TASK_SUCCESS, VALUE} from "./action";
const initialState = {
    tasks: [],
    currentTask: '',
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case VALUE:
            return {...state, currentTask: state.currentTask = action.value};
        case FETCH_TASK_SUCCESS:
            const tasks = [];
            for (let key in action.tasks) {
                tasks.unshift({...action.tasks[key], id: key});
            }
            return {...state, tasks: tasks};
        default:
            return state;
    }
};

export default reducer;