import React, { Component } from 'react';
import TodoList from './containers/TodoList/TodoList';
import './App.css';

class App extends Component {
  render() {
    return (
      <TodoList/>
    );
  }
}

export default App;
